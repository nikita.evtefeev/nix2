<?php

namespace Evtefeev\Framework\Views;

class View
{
    public array $route = [];
    public string $view;

    /**
     * @param array $route
     * @param string $view
     */
    public function __construct(array $route, string $view = '')
    {
        $this->route = $route;
        $this->view = $view;
    }

    /**
     * @param $data
     * @return void
     */
    public function render($data): void
    {
        if (is_array($data)) {
            extract($data);
        }
        $fileView = ROOT . '/App/Views/' . $this->view . '.php';


        ob_start();
        if (is_file($fileView)) {
            $context = $data;
            require $fileView;
        } else {
            echo '<h1>Not found: ' . $fileView . '</h1>';
        }
        $content = ob_get_clean();
        $fileLayout = ROOT . '/App/Views/layouts/' . 'default' . '.php';
        if (is_file($fileLayout)) {
            require $fileLayout;
        } else {
            echo 'Not found loyaut: ' . $fileLayout;
        }
//        echo $content;
    }

}
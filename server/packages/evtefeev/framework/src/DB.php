<?php

namespace Evtefeev\Framework;

use PDO;

class DB
{

    protected object $pdo;
    protected static $instance;

    protected function __construct()
    {
        $db = require ROOT . '/config/database.php';
//        print_r($db);
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
//            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ];
        $this->pdo = new PDO($db['dns'], $db['username'], $db['password'], $options);
    }

    /**
     * @return void
     */
    protected function __clone(): void
    {
        // TODO: Implement __clone() method.
    }

    /**
     * @return DB
     */
    public static function instance(): DB
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @param $sql
     * @param array $param
     * @return array
     */
    public function query($sql, array $param = []): array
    {
        $PDOStatement = $this->pdo->prepare($sql);
        $result = $PDOStatement->execute($param);
        if ($result !== false) {
            return $PDOStatement->fetchAll();
        } else {
            echo 'Query not found: ' . $sql;
            return [];
        }

    }

    public function queryID($sql, array $param = []): int
    {
//        dump($sql);
        $PDOStatement = $this->pdo->prepare($sql);
        $result = $PDOStatement->execute($param);
        if ($result !== false) {
            return $this->pdo->lastInsertId();
        } else {
            echo 'Query not found: ' . $sql;
            return -1;
        }
    }
}
<?php

namespace Evtefeev\Framework\Controllers;

use Evtefeev\Framework\Models\Model;
use Evtefeev\Framework\Views\View;

/**
 * Class Controller.
 *
 */
abstract class Controller
{
    public array $route = [];

    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * @param string $viewPath
     * @param array $data
     * @return bool
     */
    public function view(string $viewPath, array $data = []): bool
    {


        $viewObject = new View($this->route, $viewPath);
        if (!empty($this->data)) {

            $data = array_merge($data, $this->data);
        }
//        else {
//
//        }
//        dumpd($data);
        $viewObject->render($data);
        return true;
    }

}
<?php

function dump($array): void
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

function dumpd($array): void
{
    dump($array);
    die();
}

function redirect($path)
{
    $host = $_SERVER['HTTP_HOST'];
    $header = 'Location: http://' . $host . '/' . $path;
//    dumpd($header);
    header($header);
}

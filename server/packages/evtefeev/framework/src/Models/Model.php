<?php

namespace Evtefeev\Framework\Models;

use Evtefeev\Framework\DB;

abstract class Model
{
    private static DB $pdo;
    protected string $table = '';
    protected bool $created_at = false;
    protected bool $updated_at = false;

    protected array $tableColumns = [];


    public function __construct()
    {
        self::$pdo = DB::instance();

    }

    /**
     * @param string $table table
     * @param array|string $columns columns
     * @param string $param param or params
     * @param string $value value
     * @return array
     */
    protected function select(string $table, array|string $columns, string $param, string $value, string $order = ''): array
    {
        if (is_array($columns)) {
            $columns = implode(', ', $columns);
        }
        $orders = '';
        if ($order != '') {
            $ask = $order[0] == '-' ? ' DESC' : ' ASC';
            $order = ltrim($order, '-');
            $orders = " ORDER BY $order $ask";
        }
        $sql = "SELECT " . $columns . " FROM $table WHERE $param = '$value'" . $orders;

//        dump($sql);
        $res = self::$pdo->query($sql);
//        if (count($res) == 1) {
//            return $res[0];
//        }
        return $res;
    }

    /**
     * @param string $table table
     * @param string $column column
     * @param string $param param
     * @param string $value value
     * @return string
     */
    protected function selectVal(string $table, string $column, string $param, string $value): string
    {
        return $this->select($table, $column, $param, $value)[0][$column] ?? '';
    }

    /**
     * @param array $params
     * @return int
     */
    protected function insert(array $params): int
    {
        $array = array_map(function ($val) {
            $val = str_replace("'", "''", $val);
            return htmlspecialchars($val);
        }, $params);
        if ($this->created_at && $this->updated_at) {
            $array['created_at'] = date('Y-m-d H:i:s');
            $array['updated_at'] = date('Y-m-d H:i:s');

        }
        $keyString = implode(', ', array_keys($array));
        $valuesString = implode('\', \'', array_values($array));
        $sql = "INSERT INTO " . $this->table . " ($keyString) VALUES ('$valuesString')";
        return self::$pdo->queryId($sql);
    }

    /**
     * @param string $column
     * @param string $value
     * @return array array
     */
    protected function delete(string $column, string $value): array
    {
        $sql = "DELETE FROM $this->table WHERE $column = $value";
        return self::$pdo->query($sql);
    }

    /**
     * @param string $search_column
     * @param string $search_value
     * @param array $params
     * @return array
     */
    protected function update(string $search_column, string $search_value, array $params): array
    {
        if ($this->updated_at) {
            $params['updated_at'] = date('Y-m-d H:i:s');
        }

        $set = '';
        foreach ($params as $index => $param) {
            $set .= "$index = '$param', ";
        }
        $set = rtrim($set, ', ');

        $sql = "
        UPDATE $this->table
        SET $set
        WHERE $search_column = $search_value;
        ";
//        dump($sql);
        return self::$pdo->query($sql);

    }

}
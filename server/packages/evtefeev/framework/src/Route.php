<?php

namespace Evtefeev\Framework;

class Route
{
    protected static array $routes = [];
    protected static array $route = [];
    protected static array $params = [];

    /**
     * @param string $url
     * @param array $route [string pattern, class class]
     * @return void
     */
    public static function add(string $url, array $route = []): void
    {
        self::$routes[$url] = $route;
    }

    /**
     * @param string $url
     * @return void
     */
    public static function dispatch(string $url): void
    {

        if (self::matchRoute($url)) {
            $controller = self::$route[0];
            if (class_exists($controller)) {
                $obj = new $controller(self::$route);
                $action = self::$route[1];

                if (method_exists($obj, $action)) {
                    if (!empty(self::$params)) {
                        $obj->$action(self::$params);
                    } else {
                        $obj->$action();
                    }
                } else {
                    echo 'Method "' . $action . '" in  ' . get_class($obj) . ' not exist';
//                    dumpd($obj);
                }
//                var_dump($obj);
            } else {
                echo 'Controller' . $controller . ' not found';
            }
        } else {
            http_response_code(404);
            require '404.php';
        }


    }

    /**
     * @param string $url
     * @return bool
     */
    public static function matchRoute(string $url): bool
    {


        foreach (self::$routes as $pattern => $route) {

            if (str_contains($pattern, '$')) {
//                dump($pattern);
                $params = explode('$', $pattern);
                $param = array_pop($params);
                $values = explode('/', $url);
                $value = array_pop($values);
                $check_url = rtrim($url, '/' . $value) . '/';
                self::$params[$param] = $value;
                $params = $params[0];
//                dump($params);
//                dump($url);
            } else {
                $params = $pattern;
                $check_url = $url;
            }

            if ($params == $check_url) {
//                dump('work');
                self::$route = $route;
                return true;
            }
//            if($pos1 !== false){
//                return true;
//            }
        }
        return false;
    }

}



<?php

ob_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>404 Not Found</title>
</head>
<body>
<h1>Page not found</h1>
</body>
</html>

<?php

$content = ob_get_clean();
$fileLayout = ROOT . '/App/Views/layouts/' . 'default' . '.php';
if (is_file($fileLayout)) {
    require $fileLayout;
} else {
    echo 'Not found loyaut: ' . $fileLayout;
}

?>
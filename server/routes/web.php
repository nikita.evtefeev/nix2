<?php

use App\Controllers\ArticleController;
use App\Controllers\MainController;
use App\Controllers\UserController;
use Evtefeev\Framework\Route;

Route::add('/', [MainController::class, 'index']);

Route::add('/register', [UserController::class, 'register']);
Route::add('/login', [UserController::class, 'login']);
Route::add('/logout', [UserController::class, 'logout']);


Route::add('/user', [UserController::class, 'index']);

//CRUD
Route::add('/user/create', [UserController::class, 'create']);
Route::add('/user/update', [UserController::class, 'update']);
Route::add('/user/delete', [UserController::class, 'delete']);
Route::add('/user/articles/$id', [ArticleController::class, 'user']);

Route::add('/article', [ArticleController::class, 'index']);
Route::add('/article/$id', [ArticleController::class, 'article']);

//CRUD
Route::add('/article/create', [ArticleController::class, 'create']);
Route::add('/article/update/$id', [ArticleController::class, 'update']);
Route::add('/article/delete/$id', [ArticleController::class, 'delete']);



/**
 *
 *
 * 1. user articles
 * 2.
 *
 *
 *
 */
<?php

namespace App\Models;

use Evtefeev\Framework\Models\Model;

class ArticleModel extends Model
{
    protected string $table = 'articles';
    protected bool $created_at = true;
    protected bool $updated_at = true;
    protected array $tableColumns = ['id', 'title', 'description', 'tag','created_at', 'updated_at', 'user_id'];
    protected array $showColumns = ['id', 'title', 'description', 'tag','created_at', 'updated_at', 'user_id'];

    public function getAll($user_id = ''): array
    {
        if($user_id==''){
            return $this->select($this->table, $this->showColumns, 1, 1, '-created_at');

        } else {
            return $this->select($this->table, $this->showColumns, 'user_id', $user_id, '-created_at');
        }
    }

    public function getOne($id): array
    {
        $res = $this->select($this->table, $this->showColumns, 'id', $id);
        if (count($res) == 1){
            return $res[0];
        }
        return $res;
    }

    public function create(array $data): int
    {
        $data['user_id'] = $_SESSION['user_id'];
        return $this->insert($data);

    }

    public function getAuthor($id):array
    {
        return $this->select('users', ['id', 'email'],'id', $id)[0];
    }

    public function edit($id, $data)
    {
        $this->update('id', $id, $data);
    }


    public function remove($id)
    {
        $this->delete('id', $id);
    }
}
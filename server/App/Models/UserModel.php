<?php

namespace App\Models;

use Evtefeev\Framework\Models\Model;
use PDOException;

class UserModel extends Model
{
    protected string $table = 'users';
    protected bool $created_at = true;
    protected bool $updated_at = true;

    protected array $tableColumns = ['id', 'email', 'password', 'created_at', 'updated_at'];
    protected array $showColumns = ['id', 'email', 'created_at', 'updated_at', 'gender_id'];


    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->select($this->table, $this->showColumns, 1, 1);
    }

    /**
     * @param $id
     * @return array
     *
     */
    public function getOne($id): array
    {
        $user = $this->select($this->table, $this->showColumns, 'id', $id)[0];
        $gender = $this->getGender($id);
        unset($user['gender_id']);
        $user['gender'] = $gender;
        return $user;
    }

    /**
     * @param string $id
     * @return string
     */
    public function getGender(string $id): string
    {
        $gender_id = $this->selectVal($this->table, 'gender_id', 'id', $id);
        return $this->selectVal('gender', 'value', 'id', $gender_id);

    }

    public function create(array $data): int
    {
        try {
            return $this->insert($data);
        } catch (PDOException $ex) {
//            dumpd($ex);
            return -1;
        }

    }

    public function checkLogin($credentials): int
    {
        $email = $credentials['email'];
        $pass = $credentials['password'];
        $hash = $this->selectVal($this->table, 'password', 'email', $email);
        if (password_verify($pass, $hash)) {
            return $this->selectVal($this->table, 'id', 'email', $email);
        } else {
            return -1;
        }
    }

    public function remove(int $user_id)
    {
        $this->delete('id', $user_id);
    }

    public function edit(array $data)
    {
        $this->update('id', $_SESSION['user_id'], $data);
        redirect('user');
    }
}
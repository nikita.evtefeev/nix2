<?php

namespace App\Controllers;


use App\Models\ArticleModel;
use App\Models\UserModel;

class ArticleController extends BaseController
{

    protected ArticleModel $model;


    public function __construct($route)
    {
        parent::__construct($route);
        $this->model = new ArticleModel();
    }

    public function index()
    {
        $context = $this->model->getAll();
        return $this->view('article/index', $context);
    }

    public function article($params)
    {
        $id = $params['id'];
        $data = $this->model->getOne($id);
        $title = $data['title'];
        $author = $this->model->getAuthor($data['user_id']);
        $params = ['data', 'title', 'author'];
        return $this->view('article/index', compact($params));
    }

    public function create()
    {
        if ($_POST) {
            $id = $this->model->create($_POST);
            redirect('article/' . $id);
        }
        if (isset($_SESSION['user_id'])){
            return $this->view('article/create', ['title' => 'Create Article']);
        } else {
            redirect('login');
        }
        return true;
    }

    public function user($params)
    {
        $id = $params['id'];
        $articles = $this->model->getAll($id);
        return $this->view('index', ['title' => 'User articles', 'articles' => $articles]);
    }

    public function update($params)
    {
        $id = $params['id'];
        if ($_POST) {
            $this->model->edit($id, $_POST);
            redirect('article/' . $id);
        }
        $data = $this->model->getOne($id);
        return $this->view('article/update', ['title' => 'Edit article', 'data' => $data]);
    }

    public function delete($params)
    {
        $id = $params['id'];
        $this->model->remove($id);
        return $this->view('article/delete', ['title' => 'Delete article', 'message' => 'Successfully removed']);
    }
}
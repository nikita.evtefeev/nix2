<?php

namespace App\Controllers;

use App\Models\UserModel;

class UserController extends BaseController
{
    protected array $data;
    protected UserModel $model;

    public function __construct($route)
    {
        parent::__construct($route);
        $this->model = new UserModel();
    }

    public function index()
    {

        if (isset($_SESSION['user_id'])) {
            $user = $this->model->getOne($_SESSION['user_id']);
            $title = 'Cabinet';
            return $this->view('/user/index', compact(['user', 'title']));

        } else {
            return $this->view('/user/login');
        }

    }

    public function register($text = '')
    {
        $title = 'register';
        $data = ['title', 'text'];
        return $this->view('/user/register', compact($data));
    }

    public function create(): void
    {
        $data = $_POST;
//        dump($data);
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
//        dump($data);
        $id = $this->model->create($data);
//        dump($id);
        if ($id != -1) {
            $_SESSION['user_id'] = $id;
            $this->index();
        } else {
            $this->register('Email already exist');
        }

    }

    public function update()
    {
        if ($_POST){
            $this->model->edit($_POST);
        }
        $user = $this->model->getOne($_SESSION['user_id']);
        return $this->view('/user/update', ['title' => 'Edit user info', 'user' => $user]);

    }

    public function delete()
    {
        if(isset($_SESSION['user_id'])){
            $this->model->remove($_SESSION['user_id']);
            unset($_SESSION['user_id']);
            return $this->view('/user/delete', ['message' => 'Successfully deleted']);
        } else {
            return $this->view('/user/delete', ['messageEr' => 'User not found']);
        }

    }

    public function set(array $data)
    {
        $this->data = $data;
    }


    public function logout()
    {
        unset($_SESSION['user_id']);
        header('Location: login');

    }

    public function login()
    {
        if ($_POST) {
            $id = $this->model->checkLogin($_POST);
            if ($id != -1) {
                $_SESSION['user_id'] = $id;
                redirect('user');
            } else {
                return $this->view('/user/login', ['title' => 'Login', 'error' => 'Invalid email or password']);
            }
        }
        return $this->view('/user/login', ['title' => 'Login']);
    }
}
<?php

namespace App\Controllers;

use App\Models\ArticleModel;

class MainController extends BaseController
{
    public function index()
    {
        $articleModel = new ArticleModel();
        $articles = $articleModel->getAll();
        return $this->view('/index', compact(['articles']));

    }
}
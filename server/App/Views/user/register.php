
<h1>Register</h1>
<?php
if(isset($text) && $text!=''){
    echo '<div class="alert alert-warning" role="alert">'.$text.'</div>';
}
?>


<form action="/user/create" method="post">
     <div class="col-3">
        <div class="mb-3 ">
            <label for="email" class="form-label">Email:</label>
            <input type="email" name="email" class="form-control" id="email">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password:</label>
            <input type="password" name="password" class="form-control" id="password">
        </div>
        <div class="mb-3">
            <label class="form-label">Gender:</label><br>
            <input type="radio" id="html" name="gender_id" value="1" checked="checked" class="form-check-input">
            <label for="html" class="form-check-label">Male</label><br>
            <input type="radio" id="css" name="gender_id" value="2" class="form-check-input">
            <label for="css" class="form-check-label">Female</label><br>
        </div>
         <div class="mb3">
             <button type="submit" class="btn btn-success">Send</button>

         </div>
    </div>

</form>

<style>
    .info {
        margin-left: 20px;
    }
</style>

<h2>Your info:</h2>

<div class="info">
    <?php
    //
    if ($user ?? false) {
        foreach ($user as $key => $val) {
            echo "$key: <b>$val</b> <br>";

        }
    } else {
        echo 'User not found';
    }


    ?>
    <br>
    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">Remove account</button>
    <a type="button" class="btn btn-secondary" href="user/update">Edit</a>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Remove your account?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    <a type="button" class="btn btn-danger" href="user/delete">Yes</a>
                </div>
            </div>
        </div>
    </div>


</div>

<h1>Login</h1>

<div>

    <?php
    if (isset($error)) {

        echo '<div class="alert alert-danger" role="alert">
' . $error . '</div>';

    }
    ?>
</div>
<form action="/login" method="post">

    <div class="col-3">
        <div class="mb-3 ">
            <label for="email" class="form-label">Email:</label>
            <input type="email" name="email" class="form-control" id="email">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password:</label>
            <input type="password" name="password" class="form-control" id="password">
        </div>
        <button type="submit" class="btn btn-success">Login</button>
    </div>

</form>


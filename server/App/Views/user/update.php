<!--<form action="/user/update" method="post">-->
<!--    <label>-->
<!--        Email: <br>-->
<!--        <input type="email" name="email">-->
<!--    </label><br>-->
<!--    <label>-->
<!--        Password: <br>-->
<!--        <input type="password" name="password">-->
<!--    </label><br>-->
<!--    <input type="radio" id="male" name="gender_id" value="1" checked="checked">-->
<!--    <label for="male">Male</label><br>-->
<!--    <input type="radio" id="female" name="gender_id" value="2">-->
<!--    <label for="female">Female</label><br>-->
<!---->
<!--    <br>-->
<!--    <button type="submit">Send</button>-->
<!--</form>-->
<!---->
<!---->
<!--<h1>Register</h1>-->
<?php
if(isset($text) && $text!=''){
    echo '<div class="alert alert-warning" role="alert">'.$text.'</div>';
}
if(empty($user)){
    echo 'user not found';
    die();
}
?>


<form action="/user/update" method="post">
    <div class="col-3">
        <div class="mb-3 ">
            <label for="email" class="form-label">Email:</label>
            <input type="email" name="email" class="form-control" id="email" value="<?=$user['email'] ?>">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password:</label>
            <input type="password" name="password" class="form-control" id="password">
        </div>
        <div class="mb-3">
            <label class="form-label">Gender:</label><br>
            <input type="radio" id="html" name="gender_id" value="1" checked="checked" class="form-check-input">
            <label for="html" class="form-check-label">Male</label><br>
            <input type="radio" id="css" name="gender_id" value="2" class="form-check-input">
            <label for="css" class="form-check-label">Female</label><br>
        </div>
        <div class="mb3">
            <button type="submit" class="btn btn-success">Send</button>

        </div>
    </div>

</form>

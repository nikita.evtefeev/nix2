<h1>Create New Article</h1>


    <form action="/article/create" method="post">
        <div class="form-group">
            <label for="inputTitle">Title:</label>
            <input id="inputTitle" type="text" class="form-control" name="title">
        </div>
        <br>
        <div class="form-group">
            <label for="inputDescription">Description:</label>
            <textarea class="form-control" id="inputDescription" rows="3" name="description"></textarea>
        </div>
        <div class="form-group">
            <label for="inputTag">Tag:</label>
            <input id="inputTag" type="text" class="form-control" name="tag">
        </div>
        <br>
        <button type="submit" class="btn btn-primary mb-2">Create</button>
    </form>


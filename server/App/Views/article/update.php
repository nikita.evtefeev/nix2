<?php
if (empty($data)) {
    http_response_code(404);
    require '404.php';
    die();
}
?>
<h1>Edit article</h1>

<form action="/article/update/<?= $data['id'] ?>" method="post">
    <div class="form-group">
        <label for="inputTitle">Title:</label>
        <input id="inputTitle" type="text" class="form-control" name="title" value="<?= $data['title'] ?>"">
    </div>
    <br>
    <div class="form-group">
        <label for="inputDescription">Description:</label>
        <textarea class="form-control" id="inputDescription" rows="3" name="description"><?= $data['description'] ?></textarea>
    </div>
    <div class="form-group">
        <label for="inputTag">Tag:</label>
        <input id="inputTag" type="text" class="form-control" name="tag" value="<?= $data['tag'] ?>"">
    </div>
    <br>
    <button type="submit" class="btn btn-primary mb-2">Save changes</button>
</form>

<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <title><?= $title ?? 'Welcome!!!' ?></title>
</head>
<body class="d-flex flex-column h-100">


<!-- Begin page content -->
<main class="flex-shrink-0">

    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Simple Articles</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/article/create">Create article</a>
                    </li>
                    <?php
                    if (@$_SESSION['user_id']) {
                        echo '<li class="nav-item">
                        <a class="nav-link" href="/user">Cabinet</a>
                    </li>';
                        echo '<li class="nav-item">
                        <a class="nav-link" href="/user/articles/'.$_SESSION['user_id'].'">My articles</a>
                    </li>';
                        echo '<li class="nav-item">
                        <a class="nav-link" href="/logout">Logout</a>
                    </li>';

                    } else {
                        echo '<li class="nav-item">
                        <a class="nav-link" href="/login">Login</a>
                    </li>';
                        echo '<li class="nav-item">
                        <a class="nav-link" href="/register">Register</a>
                    </li>';

                    }
                    ?>


                </ul>
            </div>
        </div>
    </nav>


    <div class="container content">
        <section>
            <div class="container">
                <div class="mt-2">
                    <?php echo $content ?? 'No content' ?>

                </div>

            </div>
        </section>

    </div>
</main>

<footer class="footer mt-auto py-1 bg-light">

    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.1);">
        © 2022 Copyright:
        <a class="text-dark" href="https://evtefeev.ml/">Evtefeev.ml</a>
    </div>

</footer>

</body>
</html>